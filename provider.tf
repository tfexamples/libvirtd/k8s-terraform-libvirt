terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.7.0"
    }
  }
}

# Libvirt system
# provider "libvirt" {
#   uri = "qemu+ssh://user@ip:22/system?keyfile=$HOME/.ssh/ssh_private_key"
# }

# Libvirt system
provider "libvirt" {
  uri = "qemu:///system"
}
