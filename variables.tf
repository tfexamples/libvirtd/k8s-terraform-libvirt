variable "os_image" {
  type        = string
  description = "OS image"
  default     = "https://download.rockylinux.org/pub/rocky/9/images/x86_64/Rocky-9-GenericCloud.latest.x86_64.qcow2"
}

variable "ssh_key" {
  type        = string
  description = "SSH key"
  default     = "terraform-libvirt.pub"
}

variable "masters" {
  type        = number
  description = "Number of master instances"
  default     = 1
}
variable "workers" {
  type        = number
  description = "Number of workers instances"
  default     = 3
}

variable "masters_vcpu" {
  type        = number
  description = "Number of vCPU of master instances"
  default     = 2
}
variable "workers_vcpu" {
  type        = number
  description = "Number of vCPU of workers instances"
  default     = 2
}

variable "masters_memory" {
  type        = number
  description = "master instances RAM"
  default     = 2048
}

variable "workers_memory" {
  type        = number
  description = "worker instances RAM"
  default     = 2048
}

variable "disk_size" {
  type        = number
  description = "Instances disk size"
  default     = 85899345920
}
